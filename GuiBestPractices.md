# GUI Best Practices Report

----
## 1. Communicate

The word communication refers to an application that is able to convey information and ideas to the user. It is important to build an application that communicates well, so that we are able to direct the user on how to use the app (the input), as well as give an indication on the current status of the app (the output).

Communication can be both written and visual, and can refer to any aspect of an application that provides the user with some sort of information.

*Examples of written communication include:*

* Using clear labels and explanations
* Displaying error messages
* Using example input in text fields

*Examples of visual communication include:*

* Showing loading screens, progress bars or indicators
* Making use of different fonts, colours and imagery to direct the user's attention

It is important to note that all methods of communication carry different advantages and disadvantages, and not all forms of communication will be suitable to use within a given situation. With this in mind, it is very important to always try and select the communication methods that are the best fit for the task, as this will better direct the user. An example of this is how using a progress bar is much better for communicating a load status than written text. This is as a loading bar visually provides more information to the user than written text can possibly even begin to describe. In relation to this, it is very important to always keep the goals/outcomes of the application in mind, when designing the communication methods.

**References:**

>http://www.ambysoft.com/essays/userInterfaceDesign.html
http://web.cs.wpi.edu/~matt/courses/cs563/talks/smartin/int_design.html
https://www.usability.gov/what-and-why/user-interface-design.html
http://blog.teamtreehouse.com/10-user-interface-design-fundamentals
https://www.elegantthemes.com/blog/resources/10-rules-of-good-ui-design-to-follow-on-every-web-design-project
http://www.goodui.org/

----
## 2. Be Consistent

Consistency refers to the repetition of common themes and elements, throughout an application. It is important to incorporate elements of consistency within an application, so that the application feels unified to the user. An app that does not incorporate consistency can feel very disjointed and be difficult to use. As a result, users may be discouraged from using the application altogether.

Currently there are two main types of consistency: internal and external. Internal consistency refers to consistency within an application; whereas external consistency refers to consistency within different applications of the same type/nature. Both types of consistency are very important in maintaining the overall flow and feel of an application, and so both types should be used wherever possible.

*Examples of internal consistency include:*

* Making elements that respond similarly, look similar
* Keeping identical elements in the same place on every page/window of the application
* Keeping the same font and color scheme throughout the application
* Giving every page/window of the application the same base design 
* Repeating important taglines, phrases or imagery

*Examples of external consistency include:*

* Using common features/elements that are present in other applications of the same type
* Using similar icons to other applications, to perform the same functions

**References:**

>http://www.ambysoft.com/essays/userInterfaceDesign.html
http://web.cs.wpi.edu/~matt/courses/cs563/talks/smartin/int_design.html
https://www.usability.gov/what-and-why/user-interface-design.html
http://blog.teamtreehouse.com/10-user-interface-design-fundamentals
https://www.elegantthemes.com/blog/resources/10-rules-of-good-ui-design-to-follow-on-every-web-design-project
http://www.goodui.org/

----
## 3. Be Logical

A logical application is one that makes common sense, and follows what would be expected by the user of the application. It is important to be logical when designing an application, as it will greatly improve the usability of the application for the user. This is as a logical application will reduce the need for instructions: effectively allowing the user to make educated guesses on how to use the application instead.

*Examples of a logical application include:*

* Grouping similar elements
* Making use of visual hierarchies e.g. drop-down menus.
* Using containers to encase other elements effectively
* Choosing the correct elements/fields for a particular task
* Incorporating elements of consistency

**References:**

>http://www.ambysoft.com/essays/userInterfaceDesign.html
https://www.usability.gov/what-and-why/user-interface-design.html
http://blog.teamtreehouse.com/10-user-interface-design-fundamentals
http://beijerinc.com/pdf/whitepaper/interface_design_best_practices.pdf
https://www.elegantthemes.com/blog/resources/10-rules-of-good-ui-design-to-follow-on-every-web-design-project
https://uxplanet.org/responsive-design-best-practices-c6d3f5fd163b
http://www.goodui.org/

----
## 4. Be Familiar

An application that makes use of familiarity is one that incorporates language and elements that are already well-known to the users. Familiarity is important within an application, as it also reduces the need for instructions within an application. This is as knowledge on certain features/elements of the application, becomes common sense to the users of the application, and this makes the application a lot easier to use.

*Examples of familiarity include:*

* Using language and terms that are understood by the users
* Being consistent with the real-world
* Using commonly understood or universal icons, features & imagery
* Using features and elements relevant to the intended users

**References:**

>http://web.cs.wpi.edu/~matt/courses/cs563/talks/smartin/int_design.html
https://www.usability.gov/what-and-why/user-interface-design.html
http://blog.teamtreehouse.com/10-user-interface-design-fundamentals
https://www.elegantthemes.com/blog/resources/10-rules-of-good-ui-design-to-follow-on-every-web-design-project
http://www.goodui.org/

----
## 5. Keep it Simple

A simple application is one that is easy to use, because it does not contain features or design elements that are unnecessary to the functionality of the application. Having simplicity in an application is very important, as it stops the user from becoming overloaded, confused and frustrated. This is something that often happens with applications that expose too much unnecessary information, or offer too many unnecessary features at once. 

*Examples of simplicity include:*

* Only showing the necessary features
* Making use of drop-downs and accordions, to hide information until it is needed
* Shortening descriptions and explanations, to only give the key points
* Avoiding general on-screen clutter
* Making use of the most effective tools for the task at hand, to aid ease of use
* Making use of white-space, to direct the user's attention 

**References:**

>http://www.ambysoft.com/essays/userInterfaceDesign.html
http://web.cs.wpi.edu/~matt/courses/cs563/talks/smartin/int_design.html
https://www.usability.gov/what-and-why/user-interface-design.html
https://msdn.microsoft.com/en-us/library/dn742474.aspx
http://blog.teamtreehouse.com/10-user-interface-design-fundamentals
http://beijerinc.com/pdf/whitepaper/interface_design_best_practices.pdf
https://www.elegantthemes.com/blog/resources/10-rules-of-good-ui-design-to-follow-on-every-web-design-project
http://www.goodui.org/

----
## 6. Be Flexible

A flexible application is one that allows users to perform a number of different functions, and provides multiple ways to doing so. As such, a flexible application aids ease of use, as it is less restrictive on how the user can perform a specific function, and what they can achieve with the output. 

*Examples of flexibility include:*

* Being able to be used by a multitude of audiences
* Providing alternate interfaces/views for a specific feature
* Providing multiple ways of inputting information into a particular feature e.g. input fields and buttons
* Providing multiple ways of displaying or modifying the output of a particular feature e.g. graphs and filters
* Expecting users to make mistakes, and providing ways to rectify those mistakes

**References:**

>http://www.ambysoft.com/essays/userInterfaceDesign.html
https://msdn.microsoft.com/en-us/library/dn742474.aspx
http://blog.teamtreehouse.com/10-user-interface-design-fundamentals
http://beijerinc.com/pdf/whitepaper/interface_design_best_practices.pdf
https://www.elegantthemes.com/blog/resources/10-rules-of-good-ui-design-to-follow-on-every-web-design-project
https://uxplanet.org/responsive-design-best-practices-c6d3f5fd163b
http://www.goodui.org/

----
## 7. Be Responsive

An application that is responsive will react to user input as it happens, in order to provide a faster response (or output) to the user. As a result, applications that are responsive aid usability, as they allow us to dynamically produce output that will better assist the user, based on their current task. This means that we can potentially reduce the workload for the user, as we can correct potential mistakes before they happen, as well as perform a variety of other actions that make the interface easier to use.

*Examples of responsiveness include:*

* Expanding a menu, by clicking a button
* Adjusting text and image sizes according to the current view of the application
* Adjusting the view of the application to match the screen size
* Validating and correcting user input as it is typed, to prevent mistakes
* Suggesting search terms, to create less work for the user
* Pre-filling certain information for the user, based on their responses

**References:**
> https://uxplanet.org/responsive-design-best-practices-c6d3f5fd163b
https://thenextweb.com/dd/2015/10/19/10-rules-of-best-practice-for-responsive-design/#.tnw_tpHX2Op3
https://www.impactbnd.com/blog/responsive-design-best-practices
http://www.goodui.org/

----
## 8. Be Visually Appealing

An application that is visually appealing and follows basic design principles is easier for the user to use. This is as visually appealing applications grab and maintain the user's attention (which can aid with navigation and memory), as well as maintain readability, scannability and legibility.

*Examples of being visually appealing include:*

* Paying attention to the size, positioning and alignment of elements
* Paying attention to the colors you're using
* Ensuring legibility and readability of features
* Matching the theme of the application to the application's intended purpose

**References:**

>http://www.ambysoft.com/essays/userInterfaceDesign.html
http://web.cs.wpi.edu/~matt/courses/cs563/talks/smartin/int_design.html
https://www.usability.gov/what-and-why/user-interface-design.html
https://msdn.microsoft.com/en-us/library/dn742474.aspx
http://beijerinc.com/pdf/whitepaper/interface_design_best_practices.pdf
https://www.elegantthemes.com/blog/resources/10-rules-of-good-ui-design-to-follow-on-every-web-design-project
http://www.goodui.org/

----
## Peer-Review

I believe that the work Rhiannon has done is thorough and of a good standard. It succinctly lists a lot of important design principles that as a new GUI learner, or even as an experienced GUI user, I would find helpful. It is my belief that Rhiannon deserves full marks for her report.

> "Well done" - James Beck