//Get Event Sender
let EventTarget = (e) => {
    let sender = (e && e.target) || (window.event && window.event.srcElement);
    return document.getElementById(sender.id);
}

//Verify Input
let VerifyInput = (input) => {
    let value = input;
    if (parseInt(value) || value == '') {
        //set value to a reasonable range if it is too big
        if (value > 255) {
            value = 255;
        }
        //set value to a reasonable range if it is too small
        else if (value < 0) {
            value = 0;
        }
    }
    else {
        //set value to 0 if not a number
        value = 0;
    }
    return value;
}

//Change Value Of Slider & Text Box
let SetValues = (e) => {
    //get the element changed
    let element = EventTarget(e);
    //get the element's colour group
    let colour = element.className;
    //get the value
    let value = VerifyInput(element.value);
    //set the value for the slider of that colour
    let slider = document.getElementById(colour+'slider');
    slider.value = value; 
    //set the value for the textbox of that colour
    let text = document.getElementById(colour+'text');
    text.value = value;
}

//Get The RGB Values of All Sliders
let GetValues= () => {
    //get the value for the colours from each of the 3 sliders
    let red = document.getElementById('redslider').value;
    let green = document.getElementById('greenslider').value;
    let blue = document.getElementById('blueslider').value;
    //actually change the colours
    ChangeColour(red, green, blue);
}

//Change Colour Of Background
let ChangeColour = (red, green, blue) => {
     //get combined colour
    let colour = +red + +blue + +green;
    //change background colour
    document.body.style.backgroundColor = 'rgb('+red+','+green+','+blue+')';
    //collect the arrays of the text elements and change their colours
    ChangeText(document.getElementsByTagName('h1'), colour);
    ChangeText(document.getElementsByTagName('h3'), colour);
    ChangeText(document.getElementsByTagName('h4'), colour);
}

//Change Colour of Text
let ChangeText = (elements, colour) => {
    //cycle through the array
    for (i = 0; i < elements.length; i++) {
        if (colour > 400) {
            //make the colour black if rgb > 400
            elements[i].style.color = 'rgb(0,0,0)';
        }
        else {
            //make the colour white if rgb < 400
            elements[i].style.color = 'rgb(255,255,255)';
        }
    }
}

//Reset Everything
let Reset = () => {
    ChangeColour(255, 255, 255);
    document.getElementById('redtext').value = 255;
    document.getElementById('greentext').value = 255;
    document.getElementById('bluetext').value = 255;
    document.getElementById('redslider').value = 255;
    document.getElementById('greenslider').value = 255;
    document.getElementById('blueslider').value = 255;
}


